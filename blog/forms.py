from django import forms
from django.db import models
from .models import Blog, Comment

class CommentForm(forms.Form):
    blog = models.ForeignKey('Blog', on_delete=models.CASCADE)
    commenter = forms.CharField(
        label="", 
        widget=forms.TextInput(attrs={'placeholder': 'Name'}),
        max_length=100,
        )
    email = forms.EmailField(
        label="",
        widget=forms.TextInput(attrs={'placeholder': 'Email'})
        )
    content = forms.CharField(
        label="", 
        widget=forms.Textarea(attrs={'placeholder': 'Say Something...',
                                    'style': 'resize:none',
                                    'rows': '5',
                                    'cols': '70',
                                    }), 
        max_length=10000,
        )
    
    posted = models.DateTimeField('date published')