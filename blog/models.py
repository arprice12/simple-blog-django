import datetime
from django.db import models
from django.utils import timezone

class Blog(models.Model):
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    content = models.CharField(max_length=10000)
    posted = models.DateTimeField('date published')
      
    def short_post(self):
        x = self.content.split()
        s = x[:100]
        s = " ".join(s)
        return s
        
    def comment_count(self):
        return self.comment_set.count()
        
    def __str__(self):
        return self.content
        
class Comment(models.Model):
    blog = models.ForeignKey('Blog', on_delete=models.CASCADE)
    commenter = models.CharField(max_length=100)
    email = models.EmailField()
    content = models.CharField(max_length=10000)
    posted = models.DateTimeField('date published')
        
    def __str__(self):
        return self.content
