from django.urls import path
from . import views

app_name = 'blog'
urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
    path('tips/', views.tips, name='tips'),
    path('archive/', views.archive, name='archive'),
    path('<int:blog_id>/', views.detail, name='detail'),    
    ]
