from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from .models import Blog, Comment
from django.template import loader
from django.http import Http404
from django.urls import reverse
from django.views import generic
from .forms import CommentForm
import datetime

def index(request):
    recentBlogPosts = Blog.objects.order_by('-posted')[:3]
    context = { 'recentBlogPosts': recentBlogPosts,
    'myDate' : datetime.datetime.now(),
    }
    return render(request, 'blog/index.html', context)

def about(request):
    context = { 'myDate' : datetime.datetime.now(),
    }
    return render(request, 'blog/about.html', context)
    
def tips(request):
    context = { 'myDate' : datetime.datetime.now(),
    }
    return render(request, 'blog/tips.html', context)
    
def detail(request, blog_id):
    blogPost = get_object_or_404(Blog, pk=blog_id)
    get_comment = blogPost.comment_set.order_by('-posted')
    if request.method =='POST':
        form = CommentForm(request.POST)
        
        if form.is_valid():
            new_comment = Blog.objects.get(pk=blog_id)
            new_comment.comment_set.create(
                content = form.cleaned_data['content'],
                commenter = form.cleaned_data['commenter'],
                email = form.cleaned_data['email'],
                posted = datetime.datetime.now(),
                )
            new_comment.save()
            return HttpResponseRedirect('')
    else:
        form = CommentForm()
    
    context = {'get_comment': get_comment,
    'blog_id': blog_id,
    'myDate' : datetime.datetime.now(),
    'blogPost': blogPost,
    'form': form,
    }
    return render(request, 'blog/detail.html', context)

def archive(request):
    blog_posts = Blog.objects.order_by('-posted')
    context = {'blog_posts': blog_posts,
    'myDate': datetime.datetime.now(),
    }
    return render(request, 'blog/archive.html', context)
